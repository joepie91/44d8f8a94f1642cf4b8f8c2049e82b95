query = select("projects", anyOf([
	where(anyOf([{
		number_one: niceNumbers,
		number_two: niceNumbers
	}, {
		number_three: anyOf([ 42, column("number_one") ]),
		number_four: moreThan(1337)
	}]))
]));